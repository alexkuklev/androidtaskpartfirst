package com.das.alex.androidtaskfirst;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivityFirstTask extends AppCompatActivity {

    TextView textView;
    Button buttonSetText, buttonClearTextInView, secondTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.text_view);
        buttonSetText = findViewById(R.id.button_set_text);
        buttonSetText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText(getResources().getString(R.string.hello_world));
            }
        });
        buttonClearTextInView = findViewById(R.id.button_clear_text);
        buttonClearTextInView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText("");
            }
        });

        secondTask = findViewById(R.id.button_second_tusk);
        secondTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivityFirstTask.this, ActivitySecondTask.class);
                MainActivityFirstTask.this.startActivity(intent);
            }
        });
    }
}
