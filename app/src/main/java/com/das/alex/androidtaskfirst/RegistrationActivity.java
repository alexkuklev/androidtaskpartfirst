package com.das.alex.androidtaskfirst;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegistrationActivity extends AppCompatActivity {

    EditText emailEditText, userNameEditText, userPassword, userPasswordRep;
    Button regUserButton;
    TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        emailEditText = findViewById(R.id.email_reg);
        userNameEditText = findViewById(R.id.user_reg);
        userPassword = findViewById(R.id.password);
        userPasswordRep = findViewById(R.id.password2);
        regUserButton = findViewById(R.id.reg_user_button);
        textViewResult = findViewById(R.id.text_view_result_reg);
        regUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrationUserMethod();
            }
        });
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void registrationUserMethod(){
        if ( userPassword.getText().toString().equals(userPasswordRep.getText().toString())){
            if(isValidEmail(emailEditText.getText().toString().trim())){
                if(emailEditText.getText().length()!=0 &&
                        userNameEditText.getText().length()!=0 &&
                        userPassword.getText().length()!=0 &&
                        userPasswordRep.getText().length()!=0){
                    textViewResult.setText(getResources().getString(R.string.user_login_success));
                }else{
                    textViewResult.setText(getResources().getString(R.string.input_all_fields));
                }
            }else{
                textViewResult.setText(getResources().getString(R.string.not_correct_email));
            }
        }else{
            textViewResult.setText(getResources().getString(R.string.password_not_correct));
        }
    }
}
