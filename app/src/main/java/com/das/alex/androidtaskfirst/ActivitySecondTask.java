package com.das.alex.androidtaskfirst;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivitySecondTask extends AppCompatActivity {

    EditText editText;
    Button buttonClearEditText, buttonReturnText, buttonThirdTask;
    String valueFromEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_task);

        editText = findViewById(R.id.edit_text);
        buttonClearEditText = findViewById(R.id.button_clear_edit_text);
        buttonClearEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valueFromEditText = editText.getText().toString().trim();
                editText.setText("");
            }
        });
        buttonReturnText = findViewById(R.id.button_return_text_to_edit_text);
        buttonReturnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(valueFromEditText);
            }
        });

        buttonThirdTask = findViewById(R.id.button_third_tusk);
        buttonThirdTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivitySecondTask.this, RegistrationActivity.class);
                ActivitySecondTask.this.startActivity(intent);
            }
        });
    }
}
